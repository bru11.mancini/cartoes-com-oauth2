package br.com.itau.Cliente.repositories;

import br.com.itau.Cliente.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
