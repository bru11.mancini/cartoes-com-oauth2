package br.com.itau.Pagamento.Clients;

import br.com.itau.Pagamento.Decoder.CartaoClientDecoder;
import br.com.itau.Pagamento.security.OAuth2FeignConfiguration;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class CartaoClientConfiguration extends OAuth2FeignConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new CartaoClientDecoder();
    }


}
