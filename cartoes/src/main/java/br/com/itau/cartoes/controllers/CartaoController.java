package br.com.itau.cartoes.controllers;

import br.com.itau.cartoes.DTO.CartaoDTO;
import br.com.itau.cartoes.DTO.StatusCartaoDTO;
import br.com.itau.cartoes.models.Cartao;
import br.com.itau.cartoes.security.Usuario;
import br.com.itau.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cartao")
public class CartaoController {


    private CartaoDTO cartaoDTO;

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cartao cadastrarCartao(@RequestBody CartaoDTO cartaoDTO, @AuthenticationPrincipal Usuario usuario){
        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoDTO.getNumero());
        return cartaoService.cadastrarCartao(cartaoDTO.getClienteId(), cartao);
    }


    @GetMapping("/{idCartao}")
    public Cartao exibirCartao (@PathVariable(name = "idCartao", required = false) Integer idCartao, @AuthenticationPrincipal Usuario usuario){

        try {
            Cartao cartaoObjeto = cartaoService.buscarCartao(idCartao);
            return cartaoObjeto;

        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/cartao/{numeroCartao}")
    public Cartao exibirCartaoPorNumero (@PathVariable(name = "numeroCartao", required = false) Integer numeroCartao, @AuthenticationPrincipal Usuario usuario){

        try {
            Cartao cartaoObjeto = cartaoService.buscarCartaoPorNumero(numeroCartao);
            return cartaoObjeto;

        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PatchMapping("/{idCartao}")
    public Cartao atualizarCartao (@PathVariable(name = "idCartao") int idcartao, @RequestBody StatusCartaoDTO statusCartaoDTO, @AuthenticationPrincipal Usuario usuario){
        try {
            Cartao cartaoObjeto = cartaoService.mudarStatusCartao(idcartao, statusCartaoDTO.isAtivo());
            return cartaoObjeto;

        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
