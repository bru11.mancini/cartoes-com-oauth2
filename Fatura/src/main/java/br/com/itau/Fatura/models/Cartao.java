package br.com.itau.Fatura.models;

public class Cartao {

    private int id;

    private int idCliente;

    private int numero;

    private boolean ativo;

    public Cartao() {
    }

    public Cartao(int id, int idCliente, int numero, boolean ativo) {
        this.id = id;
        this.idCliente = idCliente;
        this.numero = numero;
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
