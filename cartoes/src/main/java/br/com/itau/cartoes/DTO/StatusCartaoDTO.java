package br.com.itau.cartoes.DTO;

public class StatusCartaoDTO {

    private boolean ativo;

    public StatusCartaoDTO() {
    }

    public StatusCartaoDTO(boolean statusCartao) {
        this.ativo = statusCartao;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
