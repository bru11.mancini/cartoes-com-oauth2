package br.com.itau.Fatura.Clients;

import br.com.itau.Fatura.models.Pagamento;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "Pagamento", configuration = PagamentoClientConfiguration.class)
public interface PagamentoClient {

    @GetMapping("/pagamentos/{idCartao}")
    List<Pagamento> getPagamentoPorId(@PathVariable int idCartao);


    @DeleteMapping("/pagamentos/idCartao/pagar")
    public void deletarComprasPorIdCartao(@PathVariable int idCartao);

}
