package br.com.itau.Fatura.models;

import javax.persistence.*;


public class Pagamento {


    private int idPagamento;

    private int idCartao;

    private String descricao;

    private double valor;

    public Pagamento() {
    }


    public Pagamento(int idPagamento, int idCartao, String descricao, double valor) {
        this.idPagamento = idPagamento;
        this.idCartao = idCartao;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(int idPagamento) {
        this.idPagamento = idPagamento;
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
