package br.com.itau.Cliente.controllers;

import br.com.itau.Cliente.models.Cliente;
import br.com.itau.Cliente.security.Usuario;
import br.com.itau.Cliente.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.bind.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody Cliente cliente, @AuthenticationPrincipal Usuario usuario){
        return clienteService.salvarCliente(cliente);
    }

    @GetMapping("/{idCliente}")
    public Cliente exibirCliente (@PathVariable(name = "idCliente", required = false) Integer idCliente, @Autowired Usuario usuario){
        try {
            Cliente clienteObjeto = clienteService.buscarCliente(idCliente);
            return clienteObjeto;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }
}
