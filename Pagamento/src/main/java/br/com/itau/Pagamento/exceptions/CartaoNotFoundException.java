package br.com.itau.Pagamento.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cartão não encontrado. Mensagem vinda da CartaoeNotFoundException")
public class CartaoNotFoundException extends RuntimeException {
}
