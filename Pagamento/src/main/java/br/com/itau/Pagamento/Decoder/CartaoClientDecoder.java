package br.com.itau.Pagamento.Decoder;

import br.com.itau.Pagamento.exceptions.CartaoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response){
        if (response.status() == 400){
            return new CartaoNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
