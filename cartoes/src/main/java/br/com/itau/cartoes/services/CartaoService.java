package br.com.itau.cartoes.services;

import br.com.itau.cartoes.Clients.ClienteClient;
import br.com.itau.cartoes.models.Cartao;

import br.com.itau.cartoes.models.Cliente;
import br.com.itau.cartoes.repositories.CartaoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteClient clienteClient;

    public Cartao cadastrarCartao(int idCliente, Cartao cartao){
//        try {
            Cliente cliente = clienteClient.getClientePorId(idCliente);
            cartao.setIdCliente(cliente.getClienteId());
            cartao.setAtivo(false);
            Cartao cartaoObjeto = cartaoRepository.save(cartao);
            return cartaoObjeto;

//        } catch (FeignException e) {
//            throw new RuntimeException("A API de clientes não encontrou o cliente que você informou. Sinto mto");
//        }
    }

    public Cartao buscarCartao(int idCartao){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(idCartao);
        if (optionalCartao.isPresent()){
            return optionalCartao.get();
        }
        throw new RuntimeException("O cartão não foi encontrado!");
    }

    public Cartao buscarCartaoPorNumero(int numeroCartao){
        Optional<Cartao> optionalCartao = cartaoRepository.findAllByNumero(numeroCartao);
        if (optionalCartao.isPresent()){
            return optionalCartao.get();
        }
        throw new RuntimeException("O cartão não foi encontrado informando o número dele!");
    }

    public Cartao mudarStatusCartao(int idCartao, boolean statusCartao){
        Cartao cartao = buscarCartao(idCartao);
        cartao.setId(idCartao);
        cartao.setAtivo(statusCartao);
        Cartao cartaoObjeto = cartaoRepository.save(cartao);
        return cartaoObjeto;
    }
}
