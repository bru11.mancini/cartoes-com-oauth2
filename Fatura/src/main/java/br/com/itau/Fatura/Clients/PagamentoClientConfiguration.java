package br.com.itau.Fatura.Clients;

import br.com.itau.Fatura.Decoder.PagamentoClientDecoder;
import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PagamentoClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new PagamentoClientDecoder();
    }

}
