package br.com.itau.cartoes.Clients;

import br.com.itau.cartoes.Decoder.ClienteClientDecoder;
import br.com.itau.cartoes.Fallback.ClienteClientFallback;
import br.com.itau.cartoes.security.OAuth2FeignConfiguration;
import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration extends OAuth2FeignConfiguration {

    @Bean
    public ErrorDecoder getErroDecoder(){

        return new ClienteClientDecoder();
    }

    @Bean
    public Feign.Builder builder(){
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new ClienteClientFallback(), RetryableException.class)
                .build();
        return Resilience4jFeign.builder(feignDecorators);
    }
}
