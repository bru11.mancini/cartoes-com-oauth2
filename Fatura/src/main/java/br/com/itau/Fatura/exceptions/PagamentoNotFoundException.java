package br.com.itau.Fatura.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Pagamentos não encontrados. Mensagem vinda da PagamentoeNotFoundException")
public class PagamentoNotFoundException extends RuntimeException {
}
