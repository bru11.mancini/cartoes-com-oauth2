package br.com.itau.Pagamento.services;


import br.com.itau.Pagamento.Clients.CartaoClient;
import br.com.itau.Pagamento.models.Cartao;
import br.com.itau.Pagamento.models.Pagamento;
import br.com.itau.Pagamento.repositories.PagamentoRepository;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoClient cartaoClient;

    public Pagamento efetuarPagamento(int idCartao, Pagamento pagamento){
//Comentei o try e o catch porque agora pegaremos as mensagens orirundas dos Decoders
//        try {
            Cartao cartao = cartaoClient.getCartaoPorId(idCartao);
            pagamento.setIdCartao(idCartao);
            Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
            return pagamentoObjeto;

//        } catch (FeignException.BadRequest e){
//            throw new RuntimeException("A API de cartão não encontrou o cartão que você informou. Sinto muito");
//        }
    }

    public List<Pagamento> buscarPagamentoPorIdCartao(int idCartao){
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllByidCartao(idCartao);
        return (List) pagamentos;
    }

    public void apagarCompras(int idCartao){
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllByidCartao(idCartao);
        pagamentoRepository.deleteAll(pagamentos);
    }
}
