package br.com.itau.Fatura.Decoder;

import br.com.itau.Fatura.exceptions.PagamentoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class PagamentoClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response){
        if (response.status() == 400){
            return new PagamentoNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
