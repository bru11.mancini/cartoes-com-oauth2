package br.com.itau.cartoes.models;

public class Cliente {

        private int clienteId;

        private String name;

        public Cliente() {
        }

        public Cliente(int clienteId, String name) {
            this.clienteId = clienteId;
            this.name = name;
        }

        public int getClienteId() {
            return clienteId;
        }

        public void setClienteId(int clienteId) {
            this.clienteId = clienteId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
