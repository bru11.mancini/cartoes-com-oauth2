package br.com.itau.Fatura.repositories;

import br.com.itau.Fatura.models.Fatura;
import br.com.itau.Fatura.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura, Integer> {
    //Iterable<Pagamento> findAllByidCartao(Integer idCartao);

}
