package br.com.itau.Fatura.controllers;

import br.com.itau.Fatura.dto.PagamentoFaturaDTO;

import br.com.itau.Fatura.models.Fatura;
import br.com.itau.Fatura.models.Pagamento;
import br.com.itau.Fatura.services.FaturaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping()
public class FaturaController {

    @Autowired
    private FaturaService faturaService;



    @PostMapping("/fatura/{idCliente}/{idCartao}/pagar")
    @ResponseStatus(HttpStatus.CREATED)
    public Fatura efetuarPagamentoFatura(@PathVariable(name = "idCartao", required = false) Integer idCartao,
                                         @PathVariable(name = "idCliente", required = false) Integer idCliente){
        Fatura fatura = faturaService.efetuarPagamento(idCartao);
        return fatura;
    }

    @GetMapping("/fatura/{idCliente}/{idCartao}")
    public List<Pagamento> exibirFaturas (@PathVariable(name = "idCartao", required = false) Integer idCartao,
                                            @PathVariable(name = "idCliente", required = false) Integer idCliente){

        try {
            List<Pagamento> pagamentos = faturaService.buscarFatura(idCartao);
            return pagamentos;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
