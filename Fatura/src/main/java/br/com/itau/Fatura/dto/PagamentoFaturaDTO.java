package br.com.itau.Fatura.dto;

import java.time.LocalDate;

public class PagamentoFaturaDTO {

    private int id;

    private double valorPago;

    private LocalDate pagoEm;


    public PagamentoFaturaDTO() {
    }

    public PagamentoFaturaDTO(int id, double valorPago, LocalDate pagoEm) {
        this.id = id;
        this.valorPago = valorPago;
        this.pagoEm = pagoEm;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValorPago() {
        return valorPago;
    }

    public void setValorPago(double valorPago) {
        this.valorPago = valorPago;
    }

    public LocalDate getPagoEm() {
        return pagoEm;
    }

    public void setPagoEm(LocalDate pagoEm) {
        this.pagoEm = pagoEm;
    }
}
