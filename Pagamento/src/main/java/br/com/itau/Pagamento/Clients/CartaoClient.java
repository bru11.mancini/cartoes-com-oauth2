package br.com.itau.Pagamento.Clients;

import br.com.itau.Pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "Cartao", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/{idCartao}")
    Cartao getCartaoPorId(@PathVariable int idCartao);

}
