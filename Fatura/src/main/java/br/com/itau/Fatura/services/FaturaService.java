package br.com.itau.Fatura.services;

import br.com.itau.Fatura.Clients.PagamentoClient;
import br.com.itau.Fatura.models.Fatura;
import br.com.itau.Fatura.models.Pagamento;
import br.com.itau.Fatura.repositories.FaturaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class FaturaService {

    @Autowired
    FaturaRepository faturaRepository;

    @Autowired
    PagamentoClient pagamentoClient;



    public Fatura efetuarPagamento(int idCartao){
        double somaValor = 0.00;
        Fatura fatura = new Fatura();
        List<Pagamento> pagamentoObjeto = pagamentoClient.getPagamentoPorId(idCartao);
        for (int i = 0; i < pagamentoObjeto.size(); i++){
            somaValor += pagamentoObjeto.get(i).getValor();
        }

        fatura.setIdCartao(idCartao);
        fatura.setValorPagamemnto(somaValor);
        fatura.setDataPagamento(LocalDate.now());

        Fatura faturaObjeto = faturaRepository.save(fatura);
        pagamentoClient.deletarComprasPorIdCartao(idCartao);


        return faturaObjeto;




    }

    public List<Pagamento> buscarFatura(int idCartao){
        Iterable<Pagamento> pagamentos = pagamentoClient.getPagamentoPorId(idCartao);
        return (List) pagamentos;
    }
}