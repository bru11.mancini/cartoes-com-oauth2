package br.com.itau.cartoes.Fallback;

import br.com.itau.cartoes.Clients.ClienteClient;
import br.com.itau.cartoes.exceptions.ClienteNotFoudException;
import br.com.itau.cartoes.exceptions.ClienteOfflineException;
import br.com.itau.cartoes.models.Cliente;

public class ClienteClientFallback implements ClienteClient {

    @Override
    public Cliente getClientePorId(int idCliente){
        throw new ClienteOfflineException();
    }
}
