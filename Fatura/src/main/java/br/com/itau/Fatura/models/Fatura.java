package br.com.itau.Fatura.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public class Fatura {

    @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idFatura;

    private int idCartao;

    private double valorPagamemnto;

    private LocalDate dataPagamento;

    public Fatura() {
    }

    public Fatura(int idFatura, int idCartao, double valorPagamemnto, LocalDate dataPagamento) {
        this.idFatura = idFatura;
        this.idCartao = idCartao;
        this.valorPagamemnto = valorPagamemnto;
        this.dataPagamento = dataPagamento;
    }

    public int getIdFatura() {
        return idFatura;
    }

    public void setIdFatura(int idFatura) {
        this.idFatura = idFatura;
    }

    public int getIdCartao() {
        return idCartao;
    }

    public void setIdCartao(int idCartao) {
        this.idCartao = idCartao;
    }

    public double getValorPagamemnto() {
        return valorPagamemnto;
    }

    public void setValorPagamemnto(double valorPagamemnto) {
        this.valorPagamemnto = valorPagamemnto;
    }

    public LocalDate getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(LocalDate dataPagamento) {
        this.dataPagamento = dataPagamento;
    }
}
